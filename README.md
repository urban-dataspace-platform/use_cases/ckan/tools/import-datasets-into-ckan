# Import Datasets Into CKAN

This script is used to import multiple datasets into CKAN at once.

## Prerequisites

- CKAN v2.9.5 with extension [ckanext-scheming](https://gitlab.com/urban-dataspace-platform/use_cases/ckan/ckanext-scheming/-/tree/udsp_master) installed:
  - custom dataset schema configured via environment variable 'scheming.dataset_schemas' and accessible via CKAN Action API Endpoint: 'scheming_dataset_schema_show'

To run this script locally:
- python 3.8
- CSV Inputfile with datasets, which is in alignement with the custom dataset schema
- APIKEY from a ckan sysadmin user

## Usage
```
usage: main.py [-h] -c CSV_FILE [-dl CSV_DELIMITER] -ak API_KEY [-e ENCODING]
               -d DOMAIN [-ah APP_HOST]

optional arguments:
  -h, --help            show this help message and exit
  -c CSV_FILE, --csv CSV_FILE
                        Full Path to CSV Input File incl. Filename (default:
                        None)
  -dl CSV_DELIMITER, --delimiter CSV_DELIMITER
                        Set Delimiter for input file (default: ,)
  -ak API_KEY, --api_key API_KEY
                        API KEY (default: None)
  -e ENCODING, --encoding ENCODING
                        Set Encoding for input file (default: UTF-8)
  -d DOMAIN, --domain DOMAIN
                        Platform Domain (i.e. stage.beispiel-stadt.de)
                        (default: None)
  -ah APP_HOST, --app_host APP_HOST
                        CKAN App Host (default: ckan)
```

## Format of the input csv file

The following rules apply to the CSV input file:

- The data needs to be in alignement with the custom dataset schema.
- The file has to be exported as CSV in Excel with Quotes and delimiter ','.
- The file needs to have two "Heading" rows:
  - first row to map the metadata fields to its type (dataset_field or resource_field):
     - all dataset fields should contain "DATENSATZ"
     - all resource fields should contain "RESSOURCE No", where 'No' is a placeholder for the ressource counter (one dataset can have 0-n ressources)
  - second row to identify the field:
     - has to be exactly the same as the configured label for the metadata field in the schema file (https://gitlab.com/urban-dataspace-platform/use_cases/ckan/ckanext-scheming/-/tree/udsp_master#label)
- The data have to be in alignement with the schema config:
  - fields, which are configured with attribute 'preset: select' or 'preset: multiple_select' needs to be filled with a valid 'label':
    - in select fields, which are configured with attribute 'choices' the valid labels can be taken from the schema file configuration
    - in select fields, which are configured with attribute 'choices_helper' the choices are configured in a CSV file uploaded as resource in CKAN. 
  - fields, which are configured with attribute 'preset: date' needs to be in the date format TT.MM.YYYY
  - fields, which are configured with attribute 'required: True' are mandatory and must not be empty.

## How it works:

The script runs through 5 steps:

1. Read schema from CKAN Action API Endpoint: 'scheming_dataset_schema_show'
2. Extract necessary configurations for the fields from the schema like choices, field_names for labels, etc.
3. Read Import Data from CSV file
4. Parse Import Data and get the internal values for the fields from the configuration (Step 2).
5. Import Datasets into CKAN

Errors can occur at step 1, 3 and 5.

The script stops on errors in step 1 and 3, since these are errors regarding the CKAN API Access or reading the import data from file.
If these steps were successfull, the script loops over all datasets and try to import them into CKAN. 
If there is an error, then it proceed with the next dataset.

For every dataset (data row (3-max) in the file it reports:
- the current number, title and unique name of the dataset
- in case of any error: error msg from ckanapi 
- in case of success: the generated dataset-id of the created dataset

example output of the script:
```bash
Step 1/5) Read schema from ckanapi ... [OK]
Step 2/5) Extract Configuration from Schema ... [OK]
Step 3/5) Read Import Data from CSV file "datasets.csv" ... [OK]
Step 4/5) Parse Import Data ... [OK]
Step 5/5) Import Data into CKAN ...
          There are 3 datasets to import ...
          Dataset 1/3: Title: "3D Daten, 2020" Name: "3d_daten_2020" ... [OK] dataset-id: 5851e8f5-f45a-4dca-abe1-cca5429fa279
          Dataset 2/3: Title: "3D Daten, 2021" Name: "3d_daten_2020" ... [Error]: {'name': ['Diese URL ist bereits vergeben.'], '__type': 'Validation Error'}
          Dataset 3/3: Title: "Testdatensatz" Name: "testdatensatz" ...  [Error]: {'notes': ['Fehlender Wert'], 'tp_kategorie_dcat': ['Fehlender Wert'], '__type': 'Validation Error'}
Step 5/5) [ERROR] 2/3 datasets could not be imported.
```