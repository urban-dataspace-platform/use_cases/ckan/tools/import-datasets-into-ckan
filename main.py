from helper import arguments, ckan_api_requests

import csv
import json


def extract_config_from_schema(ckan_url, api_key, schema_file):
    print(f'Step 2/5) Extract Configuration from Schema ... ', end='')

    fields = {}
    datastore_choices = {}

    schema_configs = [{'field_type': 'dataset', 'field_configs': schema_file['dataset_fields']},
                      {'field_type': 'resource', 'field_configs': schema_file['resource_fields']}]

    for schema_config in schema_configs:
        field_type = schema_config['field_type']
        field_configs = schema_config['field_configs']

        for field_config in field_configs:
            config = {'field_type': field_type,
                      'label': field_config['label']}
            if 'default' in field_config:
                schema_config['default'] = field_config['default']
            if 'preset' in field_config:
                if field_config['preset'] in ['multiple_select', 'select']:
                    if 'choices' in field_config:
                        config['choices'] = field_config['choices']
                    elif 'datastore_choices_resource' in field_config:
                        resource_id = field_config['datastore_choices_resource']
                        columns = field_config['datastore_choices_columns']
                        choices_for_resource = ckan_api_requests.get_datastore_choices_from_resource(ckan_url, api_key, resource_id, columns)
                        datastore_choices[resource_id] = choices_for_resource
                        config['choices'] = choices_for_resource

                config['preset'] = field_config['preset']

            fields[field_config['field_name']] = config

    #    print(f'Step 2/5) configuration: ')
    #    print(json.dumps(fields, indent=2, ensure_ascii=False))
    print(f'[OK]')

    return fields


def get_field_type(csv_data, col_index):
    type_str = csv_data[0][col_index].lower().strip()
    if type_str == 'datensatz':
        return 'dataset'
    elif type_str.startswith('ressource'):
        return 'resource'

    return None


def get_resource_number(csv_data, col_index):
    type_str = csv_data[0][col_index].lower().strip()

    parts = type_str.split('ressource')
    if len(parts) < 2:
        return 0

    resource_num = int(parts[1].lstrip())

    return resource_num


def get_label(csv_data, col_index):
    return csv_data[1][col_index].strip()


def get_data(csv_data, row_index, col_index):
    return csv_data[row_index][col_index].strip()


def get_field_name_for_label(config, label, field_type):
    if label == 'Sichtbarkeit':
        return 'private'

    if label == 'URL':
        return 'name'

    for key, value in config.items():
        if value['label'] == label and value['field_type'] == field_type:
            return key

    return None


def get_attribute_for_field(config, field_name, field):
    if field_name == 'private':
        return None

    field_config = config[field_name]
    if field in field_config:
        return field_config[field]

    return None


def read_csv_into_list(import_file, encoding, delimiter):
    print(f'Step 3/5) Read Import Data from CSV file "{import_file}" ... ', end='')

    data_array = []
    try:
        # try open CSV File
        csvf = open(import_file, encoding=encoding)
    except IOError as e: 
        print (f'[Error]: {e}')
        return None
    
    # reading data from a csv file
    with csvf:
        reader = csv.reader(csvf, delimiter=delimiter)
        # output list to store all rows
        for row in reader:
            data_array.append(row[:])

        print(f'[OK]')
        return data_array


def get_private(private):
    if private.lower() == 'öffentlich':
        return False
    elif private.lower() == 'privat':
        return True


def parse_import_data(ckan_url, api_key, csv_data, fields_config):
    metadata = []

    print(f'Step 4/5) Parse Import Data ... ', end='')

    # iterate through all metadata entries (table rows 3 - end) and append it to metadata list
    for row_index, row in enumerate(csv_data):
        # skip first two rows we don't need to get dataset_type and titles at this level
        if row_index < 2:
            continue
        # get all metadata entry
        else:
            dataset = {}
            resources = []

            resource_counter = 0
            current_resource = {}
            # iterate through table cols
            for col_index, col in enumerate(row):

                field_type = get_field_type(csv_data, col_index)
                field_label = get_label(csv_data, col_index)
                field_data = get_data(csv_data, row_index, col_index)
                field_name = get_field_name_for_label(fields_config, field_label, field_type)
                field_preset = get_attribute_for_field(fields_config, field_name, 'preset')

                if field_data != '':
                    # check if we need to convert field_data
                    # date needs to be converted from user input format TT.MM.YYYY into YYYY.MM.TT
                    if field_preset == 'date':
                        date_parts = field_data.split('.')
                        date = f'{date_parts[2]}.{date_parts[1]}.{date_parts[0]}'
                        field_data = date
                    elif field_preset in ['select', 'multiple_select']:
                        choices = fields_config[field_name]['choices']
                        for choice in choices:
                            if choice['label'] == field_data:
                                field_data = choice['value']
                                break
                    elif field_preset == 'dataset_organization':
                        owner_org = ckan_api_requests.get_organization_id(ckan_url, api_key, field_data)
                        field_data = owner_org
                    elif field_name == 'private':
                        field_data = get_private(field_data)

                    # get dataset fields
                    if field_type == 'dataset':
                        dataset[field_name] = field_data
                    # resource field ?
                    else:
                        # resource field ???
                        if field_type == 'resource':
                            if resource_counter == 0:
                                resource_counter = 1

                            current_resource_num = get_resource_number(csv_data, col_index)

                            if current_resource_num > resource_counter:
                                resources.append(current_resource)
                                current_resource = {}
                                resource_counter = current_resource_num

                            current_resource[field_name] = field_data

                        else:
                            continue

            if current_resource:
                resources.append(current_resource)

            dataset['resources'] = resources
            metadata.append({'dataset': dataset})

    #    for entry in metadata:
    #        print(json.dumps(entry, indent=2, ensure_ascii=False))

    print(f'[OK]')

    return metadata


if __name__ == '__main__':

    args = arguments.parse_arguments()
    ckan_url = "https://" + args.app_host + "." + args.domain

    # Step 1
    schema_file = ckan_api_requests.read_schema(ckan_url, args.api_key)
    if schema_file:
        # Step 2
        fields_config = extract_config_from_schema(ckan_url, args.api_key, schema_file)
        # Step 3
        csv_data = read_csv_into_list(args.csv_file, args.encoding, args.csv_delimiter)
        if csv_data:
            # Step 4
            data = parse_import_data(ckan_url, args.api_key, csv_data, fields_config)
            # Step 5
            success = ckan_api_requests.create_datasets(ckan_url, args.api_key, data)
            if success:
                exit(0)

    exit(1)

