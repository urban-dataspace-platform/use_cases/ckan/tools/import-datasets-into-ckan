import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "-c",
        "--csv",
        required=True,
        dest="csv_file",
        help="Full Path to CSV Input File incl. Filename"
    )

    parser.add_argument(
        "-dl",
        "--delimiter",
        dest="csv_delimiter",
        help="Set Delimiter for input file",
        default=","
    )

    parser.add_argument(
        "-ak",
        "--api_key",
        required=True,
        dest="api_key",
        help="sysadmin API KEY generated via CKAN UI"
    )

    parser.add_argument(
        "-e",
        "--encoding",
        dest="encoding",
        help="Set Encoding for input file",
        default="UTF-8"
    )

    parser.add_argument(
        "-d",
        "--domain",
        required=True,
        dest="domain",
        help="Platform Domain (i.e. stage.beispiel-stadt.de)"
    )

    parser.add_argument(
        "-ah",
        "--app_host",
        dest="app_host",
        help="CKAN App Host",
        default="ckan"
    )

    args = parser.parse_args()

    return args

