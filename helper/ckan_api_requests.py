from ckanapi import RemoteCKAN, NotAuthorized, NotFound, ValidationError, SearchQueryError, SearchError, CKANAPIError, \
    ServerIncompatibleError


def read_schema(ckan_url, api_key, type='dataset'):
    print(f'Step 1/5) Read schema from ckanapi ... ', end='')
    try:
        remote_ckan = RemoteCKAN(ckan_url, apikey=api_key)
        schema = remote_ckan.action.scheming_dataset_schema_show(type=type)
        print(f'[OK]')
        return schema

    except (NotAuthorized, NotFound, ValidationError, SearchQueryError, SearchError, CKANAPIError,
            ServerIncompatibleError) as e:
        print(f'[Error]: {e}')

    return None


def get_datastore_choices_from_resource(ckan_url, api_key, choice_helper_resource, choice_helper_columns):
    fields = [choice_helper_columns['value'], choice_helper_columns['label']]

    try:
        remote_ckan = RemoteCKAN(ckan_url, apikey=api_key)
        result = remote_ckan.action.datastore_search(resource_id=choice_helper_resource, fields=fields)
    except (NotAuthorized, NotFound, ValidationError, SearchQueryError, SearchError, CKANAPIError,
            ServerIncompatibleError) as e:
        print(e)
        return None

    datastore_choices = [{
        'value': r[fields[0]],
        'label': r[fields[1]]
    } for r in result['records']]

    return datastore_choices


def get_organization_id(ckan_url, api_key, org_title):
    try:
        remote_ckan = RemoteCKAN(ckan_url, apikey=api_key)
        org_list = remote_ckan.action.organization_autocomplete(q=org_title)
    except (NotAuthorized, NotFound, ValidationError, SearchQueryError, SearchError, CKANAPIError,
            ServerIncompatibleError) as e:
        print(e)
        return None

    for org in org_list:
        if org['title'] == org_title:
            return org['name']

    return None


def create_datasets(ckan_url, api_key, data):
    print(f'Step 5/5) Import Data into CKAN ...')

    dataset_counter = 0
    number_of_datasets = len(data)
    dataset_errors = 0
    print(f'          There are {number_of_datasets} datasets to import ...')

    for metadata in data:
        package = metadata['dataset']
        dataset_name = package['name']
        dataset_title = package['title']
        print(
            f'          Dataset {dataset_counter+1}/{number_of_datasets}: Title: "{dataset_title}" Name: "{dataset_name}" ... ',
            end='')
        try:
            remote_ckan = RemoteCKAN(ckan_url, apikey=api_key)
            dataset_created = remote_ckan.call_action(action="package_create", data_dict=package)
            print(f'[OK] dataset-id: {dataset_created["id"]} ')

        except (NotAuthorized, NotFound, ValidationError, SearchQueryError, SearchError, CKANAPIError,
                ServerIncompatibleError) as e:
            print(f'[Error]: {e}')
            dataset_errors += 1

        dataset_counter += 1

    if dataset_errors == 0:
        print(f'Step 5/5) [OK] {dataset_counter}/{number_of_datasets} datasets successfully imported')
        return True

    print(f'Step 5/5) [ERROR] {dataset_errors}/{number_of_datasets} datasets could not be imported.')

    return False


